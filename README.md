# SimpleUserProfile

SimpleUserProfile is a basic full-stack application running on a Tomcat local server available from Spring Boot, and using a UI created in ReactJs. <br />


//TODOS <br />
//- validate edit form <br />
//- create JESTs for PUT/GET/POST methods <br />
//- take care of the avatar (back and front end) <br />

 <br />
//FUTURE <br />
//- add a login <br />
//- take care of multiple users <br />


 <br />
//HOW TO RUN <br />
//- run the main "UserProfileApplication.java" from the backend "user-profile" in your favourite IDE supporting Maven projects <br />
//- run the front end "frontend-simple-user-profile" with npm <br />
//check the server at "localhost:8080/users" and the front-end at "localhost:3000/users"
